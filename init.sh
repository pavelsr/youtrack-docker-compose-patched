#!/usr/bin/env bash
# Clean data and conf folders. Debug and initial setup use only
YOUTRACK_HOST_DIR=/srv/youtrack
rm -rf ${YOUTRACK_HOST_DIR}/data/*
rm -rf ${YOUTRACK_HOST_DIR}/conf/*
rm -rf ${YOUTRACK_HOST_DIR}/logs/*
mkdir -p /srv/letsencrypt && mkdir -p -m 750 ${YOUTRACK_HOST_DIR}/{data,conf,backups,logs} && chown -R 13001:13001 ${YOUTRACK_HOST_DIR}
