## Почему возникает ошибка `Invalid location: Directory /opt/youtrack/data is not empty` при установке?

Нужно не помещать в `ja-netfilter` в папку `/opt/youtrack/data`


### Как правильно писать конфиг в `youtrack.jvmoptions` ?

Неправильно:

```
-jetbrains.youtrack.licenseName:"https://zhile.io"
-jetbrains.youtrack.licenseName:https://zhile.io
```

Правильно:

```
-Djetbrains.youtrack.licenseName:https://zhile.io
```
